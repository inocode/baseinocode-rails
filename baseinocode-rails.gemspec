# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'baseinocode/rails/version'

Gem::Specification.new do |spec|
  spec.name          = "baseinocode-rails"
  spec.version       = Baseinocode::Rails::VERSION
  spec.authors       = ["Mateus Nava"]
  spec.email         = ["mateus@inocode.com.br"]
  spec.summary       = %q{GEM Base para Projetos da Inocode}
  spec.homepage      = "http://www.inocode.com.br"
  spec.license       = "MIT"

  spec.files         = Dir["{lib,app}/**/*"] + ["README.md"]
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_runtime_dependency "compass-rails", "~> 2.0.2"

  spec.add_development_dependency "bundler", "~> 1.6"
  spec.add_development_dependency "rake"
end
