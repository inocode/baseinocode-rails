Turbolinks.enableProgressBar();

$(function() {
  window.App = function() {
    this.applyMasks();
    this.binds();
    this.activeMenu();
  };

  App.masks = {
    CPF: "999.999.999-99",
    CNPJ: "99.999.999/9999-99",
    CEP: "99999-999",
    DATA: "99/99/9999",
    HORA: "99:99",
    TELEFONE: "(99) 9999-9999?9",
    TELEFONE8: "(99) 9999-9999",
    CARTAO: "9 999 999 999",
    PLACA_BR: "aaa-9999",
    MAC_ADDRESS: "**:**:**:**:**:**",
    DDD: "99"
  }

  App.prototype.applyMasks = function() {
    $("input[data-mascara=cep]").mask(App.masks.CEP);
    $("input[data-mascara=data]").mask(App.masks.DATA);
    $("input[data-mascara=telefone]").mask(App.masks.TELEFONE);
    $("input[data-mascara=telefone8]").mask(App.masks.TELEFONE8);
    $("input[data-mascara=cnpj]").mask(App.masks.CNPJ);
    $("input[data-mascara=cpf]").mask(App.masks.CPF);
    $("input[data-mascara=cartao]").mask(App.masks.CARTAO);
    $("input[data-mascara=placa-br]").mask(App.masks.PLACA_BR);
    $("input[data-mascara=mac-address]").mask(App.masks.MAC_ADDRESS);
    $("input[data-mascara=hora]").mask(App.masks.HORA);
    $("input[data-mascara=ddd]").mask(App.masks.DDD);
    $("input[data-mascara=ip]").ipAddress();

    $("input[data-mascara=dinheiro]").priceFormat({
      prefix: 'R$ ',
      centsSeparator: ',',
      thousandsSeparator: '.',
      centsLimit: 2
    });

    $("input[data-mascara=decimal]").priceFormat({
      prefix: '',
      suffix: '',
      centsSeparator: ',',
      thousandsSeparator: '.',
      centsLimit: 2
    });
  }

  App.prototype.controllerActual = function() {
    return $("body").data("controller");
  }

  App.prototype.activeMenu = function() {
    $("a[data-controller=" + this.controllerActual() + "]").parent("li").addClass("active");
  }

  App.prototype.markDirty = function() {
    if (!window._isDirty) {
      $(window).bind("beforeunload", function(e){
        var message = "As alterações ainda não foram salvas";

        e = e || window.event;
        if (e) {
          e.returnValue = message;
        }

        return message;
      });
    }
    window._isDirty = true;
  }

  App.prototype.clearDirty = function() {
    window._isDirty = false;
    $(window).unbind("beforeunload");
  }

  App.prototype.getPeriodo = function(tipo) {
    var dataInicial = new Date();
    var dataFinal = new Date();

    switch (tipo) {
      case '':
        dataInicial = dataFinal = null;
        break;
      case 'HOJE':
        break;
      case 'ONTEM':
        dataInicial.setDate(dataInicial.getDate() - 1);
        dataFinal.setDate(dataFinal.getDate() - 1);
        break;
      case 'SEMANA_ATUAL':
        dataInicial.setDate(dataInicial.getDate() - dataInicial.getDay());
        dataFinal.setDate(dataInicial.getDate() + 6);
        break;
      case 'SEMANA_ANTERIOR':
        dataInicial.setDate(dataInicial.getDate() - dataInicial.getDay());
        dataFinal.setDate(dataInicial.getDate() - 1);
        dataInicial.setDate(dataFinal.getDate() - 6);
        break;
      case 'MES_ANTERIOR':
        dataInicial.setDate(1);
        dataInicial.setMonth(dataInicial.getMonth() - 1);
        dataFinal = moment(dataInicial).add('months', 1).subtract('days', 1)
        break;
      case 'MES_ATUAL':
        dataInicial.setDate(1);
        dataFinal.setMonth(dataFinal.getMonth()+1, 1);
        dataFinal.setDate(dataFinal.getDate() - 1);
        break;
      case 'PROXIMO_MES':
        dataInicial = moment().date(1).add("months", 1);
        dataFinal = moment().date(1).add("months", 2).subtract("days", 1);
        break;
      case 'ULTIMOS_3_MESES':
        dataInicial.setDate(1);
        dataInicial.setMonth(dataFinal.getMonth()-2, 1);
        dataFinal.setMonth(dataFinal.getMonth()+1, 1);
        dataFinal.setDate(dataFinal.getDate() - 1);
        break;
      case 'ANO_CORRENTE':
        dataInicial.setMonth(0, 1);
        dataFinal.setMonth(11, 31);
        break;
      case 'ULTIMOS_365_DIAS':
        dataInicial = moment(dataFinal).subtract('days', 365);
        break;
    }

    return [dataInicial, dataFinal]
  }

  App.prototype.binds = function() {
    $("[data-toggle=tooltip]").on("click", function(e) {
      e.preventDefault();
    }).tooltip();

    $("[data-form-submit]").on("click", function(e) {
      var form = $($(this).data("form-submit"));

      if (form.find("input[type='submit']").length > 0) {
        form.find("input[type='submit']").click();
      } else {
        form.submit();
      }

      e.preventDefault();
    });

    $("[data-order]").on("click", function(e) {
      var parameters = $.url().param();
      e.preventDefault();
      var order = $(this).data("order");
      var ascDesc = $(this).data("ascdesc");
      if (ascDesc == "asc") {
        order += " desc";
      } else {
        order += " asc";
      }
      var url = location.href.replace(/\?.*$/, "");
      var parametros = $.url().param();
      parametros["order"] = order;
      var flag = true;
      for (i in parametros) {
        url += (flag ? "?" : "&") + i + "=" + parametros[i];
        flag = false;
      }
      location.href = url;
    });

    $("body").on("click", "tr[data-href] td:not([data-no-href])", function(e) {
      Turbolinks.visit($(this).parents("tr:first").data("href"));
    });

    setTimeout(function() {
      $("[autofocus]:first").focus();
    }, 500);
  }
});

$(document).on("page:change", function() {
  window.app = new window.App();
});


var adjustAffix = function() {
  var widFix = $("#content-wrapper").width();
  $("#affix-actions").addClass("affix");
  $("#affix-actions").width(widFix);
}

$(window).on("load", adjustAffix);
$(window).on("resize", adjustAffix);
$(document).on("page:load", adjustAffix);

$(document).on("page:before-change", function(e) {
  if (window._isDirty && !confirm("As alterações ainda não foram salvas. Quer mesmo sair da página?")) {
    e.preventDefault();
  } else {
    app.clearDirty();
  }
});

$(document).on("page:change", function(e) {
  $("header .toggle-nav").on("click", adjustAffix);
  $("div.form form *").on("change", function() {
    app.markDirty();
  });

  $("div.form form").on("submit", function() {
    app.clearDirty();
  });
});
